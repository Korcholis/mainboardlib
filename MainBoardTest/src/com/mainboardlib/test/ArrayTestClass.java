package com.mainboardlib.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class ArrayTestClass {

	public ArrayList<Integer> list;
	public Vector<Integer> vec;
	public HashMap<Integer,Integer> map;
	
	private int max = 1000;
	
	public void arrayListTest() {
		for (int i = 0; i < max; i++) {
			list.add(i);
		}
	}

	public void vectorTest() {
		for (int i = 0; i < max; i++) {
			vec.add(i);
		}
	}

	public void hashmapTest() {
		for (int i = 0; i < max; i++) {
			map.put(i,i);
		}
	}

}
