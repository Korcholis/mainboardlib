package com.mainboardlib.test;

import com.mainboardlib.orm.MBDatabase;

public class ExampleDatabase extends MBDatabase {
	
	public ExampleDatabase() {
		super();
		try {
			addTable(new TableExample());
			addTable(new ForeignTable());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public String authority() {
		return "com.mainboardlib.test";
	}

	@Override
	public String dbName() {
		return "test.db";
	}

	@Override
	public int dbVersion() {
		return 2;
	}

}
