package com.mainboardlib.test;

//import com.mainboardlib.benchmarks.IMBTimeTesterCode;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import com.mainboardlib.benchmarks.MBBenchmarkTest;
import com.mainboardlib.benchmarks.MBBenchmarker;
import com.mainboardlib.benchmarks.MBTimeTester;
import com.mainboardlib.orm.MBModel;
import com.mainboardlib.exceptions.ModelNotSetException;
import com.mainboardlib.orm.fields.MBDatabaseField;
//import com.mainboardlib.orm.MBDatabase;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class AndroidActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		databaseExample();
		//benchmarkExample();
	}

	private void databaseExample() {
		ExampleDatabase.context = getApplicationContext();
		ExampleDatabase db = new ExampleDatabase();
		TableExample t = new TableExample();
		t.email.val("korcholis@gmail.com");
		t.name.val("Sergi Juanola");
		t.insert();
		t = new TableExample();
		t.email.val("sergi@alt120.com");
		t.name.val("Sergi Juanola");
		t.insert();
		t = new TableExample();
		t.email.val("correur1@gmail.com");
		t.name.val("Roger Tarragó");
		t.insert();
		Vector<MBModel> results = t.select("id = 1");
		for (MBModel table : results) {
			ArrayList<Field> fields = table.getMBFields();
			for (Field field : fields) {
				try {
					Log.i("MBModel", ((MBDatabaseField<?>)field.get(table)).fieldName+":\t"+((MBDatabaseField<?>)field.get(table)).val());
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		results = t.selectAll();
		for (MBModel table : results) {
			ArrayList<Field> fields = table.getMBFields();
			for (Field field : fields) {
				try {
					Log.i("MBModel", ((MBDatabaseField<?>)field.get(table)).fieldName+":\t"+((MBDatabaseField<?>)field.get(table)).val());
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		}
		TableExample table = new TableExample();
		try {
			table = ((TableExample) db.model("tableexample").selectById(1));
		} catch (ModelNotSetException e) {
			e.printStackTrace();
		}
		Log.i("MBModel", "Delete: "+table.delete());
	}

	@SuppressWarnings("unused")
	private void benchmarkExample() {

		MBBenchmarker benchmark = new MBBenchmarker("ArrayTestClass", MBTimeTester.SLOW_ACCURATE);
		benchmark.addTest(new MBBenchmarkTest<ArrayTestClass>() {
			
			public String testName() {
				return "ArrayList";
			}
			
			public void onTestStart() {
				this.object(new ArrayTestClass());
				this.object().list = new ArrayList<Integer>();
			}
			
			public void onTestRestart() {
				this.object(new ArrayTestClass());
				this.object().list = new ArrayList<Integer>();
			}
			
			public void codeGoesHere() {
				this.object().arrayListTest();
			}
		});
		benchmark.addTest(new MBBenchmarkTest<ArrayTestClass>() {
			
			public String testName() {
				return "Vector";
			}
			
			public void onTestStart() {
				this.object(new ArrayTestClass());
				this.object().vec = new Vector<Integer>();
			}
			
			public void onTestRestart() {
				this.object(new ArrayTestClass());
				this.object().vec = new Vector<Integer>();
			}
			
			public void codeGoesHere() {
				this.object().vectorTest();
			}
		});
		benchmark.addTest(new MBBenchmarkTest<ArrayTestClass>() {
			
			public String testName() {
				return "HashMap";
			}
			
			public void onTestStart() {
				this.object(new ArrayTestClass());
				this.object().map = new HashMap<Integer,Integer>();
			}
			
			public void onTestRestart() {
				this.object(new ArrayTestClass());
				this.object().map = new HashMap<Integer,Integer>();
			}
			
			public void codeGoesHere() {
				this.object().hashmapTest();
			}
		});
		benchmark.runBenchmark();
	}

}
