package com.mainboardlib.benchmarks;

import java.util.HashMap;
import java.util.Iterator;

import android.util.Log;

public class MBBenchmarker {
	private HashMap<String, MBBenchmarkTest<?>> tests;
	private String benchmarkName;
	private int iterations;
	
	public MBBenchmarker(String benchmarkName, int iterations)
	{
		tests = new HashMap<String, MBBenchmarkTest<?>>();
		this.benchmarkName = benchmarkName;
		this.iterations = iterations;
	}
	
	public void addTest(MBBenchmarkTest<?> test)
	{
		tests.put(test.testName(), test);
	}
	
	public void runBenchmark()
	{
		Runnable task;
		Iterator<String> it = tests.keySet().iterator();
		while(it.hasNext()) {	
			String key = (String) it.next();
			final MBBenchmarkTest<?> test = (MBBenchmarkTest<?>) tests.get(key);
			task = new Runnable() {
				
				public void run() {
					long t1,t2;
					long finalTime = 0;
					for (int i = 0; i < iterations; i++) {
						test.onTestStart();
						t1 = System.currentTimeMillis();
						test.codeGoesHere();
						t2 = System.currentTimeMillis();
						finalTime += (t2-t1);
						test.onTestRestart();
					}
					Log.i("MBBenchmarker", "[MBBenchmarker: "+benchmarkName+"] "+test.testName()+" took "+finalTime+"ms to run "+iterations+" iterations ("+finalTime/(float)iterations+" ms on average)");
				}
			};
			task.run();
		}
	}
}
