package com.mainboardlib.benchmarks;

public abstract class MBBenchmarkTest<T> {
	private T object;
	public abstract String testName();
	public abstract void onTestStart();
	public abstract void codeGoesHere();
	public abstract void onTestRestart();
	
	public T object()
	{
		return object;
	}
	
	public void object(T obj)
	{
		object = obj;
	}
}
