package com.mainboardlib.orm;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Vector;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.mainboardlib.orm.fields.MBDatabaseField;
import com.mainboardlib.orm.fields.MBDatabaseFieldType;
import com.mainboardlib.orm.fields.MBDate;
import com.mainboardlib.orm.fields.MBForeignField;
import com.mainboardlib.orm.fields.MBInt;
import com.mainboardlib.orm.fields.MBLong;
import com.mainboardlib.orm.fields.MBString;
import com.mainboardlib.orm.fields.MBTime;

public abstract class MBModel {
	
	public MBLong id = new MBLong("id", MBDatabase.ID_UNSET, true, true);
	private static MBDatabase parent = null;
	private static final Class<?> __classHolder = MBDatabaseField.class;
	private Uri uri;
	private String[] projection = {};
	private ArrayList<Field> fields;
	
	public MBModel()
	{
		uri = Uri.parse("content://"+parent.authority()+"/"+tableName());
	}
	
	public Uri getUri()
	{
		return uri;
	}
	
	public String[] projection()
	{
		if(projection.length == 0)
		{
			Vector<String> temp = new Vector<String>();
			ArrayList<Field> mbFields = getMBFields();
			for (Field field : mbFields) {
				try {
					if(!((MBDatabaseField<?>)field.get(this)).isReference)
						temp.add(((MBDatabaseField<?>)field.get(this)).fieldName);
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			projection = (String[]) temp.toArray(new String[temp.size()]);
		}
		return projection;
	}
	
	public static MBDatabase getParent() {
		return parent;
	}

	public static void setParent(MBDatabase p)
	{
		parent = p;
	}
	
	public abstract String tableName();
	
	public String onCreateTable(SQLiteDatabase db)
	{
		ArrayList<Field> mbFields = getMBFields();
		StringBuilder query = new StringBuilder();
		StringBuilder fkQuery = new StringBuilder();
		query.append("CREATE TABLE "+tableName()+"(\n");
		boolean notFirst = false;
		MBDatabaseField<?> dbField;
		for (Field field : mbFields) {
			dbField = null;
			try {
				dbField = ((MBDatabaseField<?>)field.get(this));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			if(!dbField.isReference)
			{
				if(notFirst)
			        query.append(",\n");
				else
					notFirst = true;
				query.append("\t"+dbField.toCreateString());
				if(field.getType() == MBForeignField.class)
				{
					if(notFirst)
				        fkQuery.append(",\n");
					else
						notFirst = true;
					fkQuery.append("\t"+((MBForeignField<?>) dbField).toSqlFkString());
				}
			}
		}
		query.append(fkQuery).append("\n);");
		db.execSQL(query.toString());
		return query.toString();
	}
	
	public ArrayList<Field> getMBFields()
	{
		if(fields == null || fields.size() == 0)
		{
			Field[] tFields = this.getClass().getFields();
			fields = new ArrayList<Field>();
			for (int i = 0; i < tFields.length; i++) {
				if(tFields[i].getType().getSuperclass() == __classHolder)
				{
					fields.add(tFields[i]);
				}
			}
		}
		return fields;
	}
	
	public Vector<MBModel> selectAll()
	{
		return select("");
	}
	
	public Cursor cursorSelectAll()
	{
		return cursorSelect("");
	}
	
	public Vector<MBModel> select(String where)
	{
		Vector<MBModel> result = new Vector<MBModel>();
		Cursor c = parent.getDb().query(tableName(), projection(), where, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast())
		{
			result.add(toModel(c));
			c.moveToNext();
		}
		c.close();
		parent.closeDb();
		return result;
	}
	
	public Cursor cursorSelect(String where)
	{
		return parent.getDb().query(tableName(), projection(), where, null, null, null, null);
	}
	
	public MBModel selectById(long id)
	{
		MBModel result = null;
		Cursor c = parent.getDb().query(tableName(), projection(), "id = "+id, null, null, null, null);
		c.moveToFirst();
		while(!c.isAfterLast())
		{
			result = toModel(c);
			c.moveToNext();
		}
		c.close();
		parent.closeDb();
		return result;
	}
	
	public Cursor cursorSelectById(long id)
	{
		return parent.getDb().query(tableName(), projection(), "id = "+id, null, null, null, null);
	}
	
	public MBModel toModel(Cursor c)
	{
		MBModel temp = null;;
		try {
			temp = this.getClass().newInstance();
		} catch (IllegalAccessException e1) {
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			e1.printStackTrace();
		}
		int current = 0;
		ArrayList<Field> mbFields = getMBFields();
		try {
			for (Field field : mbFields) {
				if(field.getType() == MBLong.class)
				{
					((MBLong)field.get(temp)).fromSqlVal(c.getLong(current++));
				}
				else if(field.getType() == MBDate.class)
				{
					((MBDate)field.get(temp)).fromSqlVal(c.getLong(current++));
				}
				else if(field.getType() == MBTime.class)
				{
					((MBTime)field.get(temp)).fromSqlVal(c.getLong(current++));
				}
				else if(field.getType() == MBString.class)
				{
					((MBString)field.get(temp)).fromSqlVal(c.getString(current++));
				}
				else if(field.getType() == MBInt.class)
				{
					((MBInt)field.get(temp)).fromSqlVal(c.getInt(current++));
				}
				else
				{
					current++;
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		return temp;
	}
	
	public long insert()
	{
		ContentValues values = new ContentValues();
		fields = getMBFields();
		for (Field field : fields) {
			MBDatabaseField<?> dbField = null;
			try {
				dbField = ((MBDatabaseField<?>) field.get(this));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			if(dbField.val() == null)
			{
				values.putNull(dbField.fieldName);
			}
			else
			{
				String type = dbField.type;
				if(type==MBDatabaseFieldType.INTEGER)
				{
					if(dbField.toSqlVal().getClass()==Long.class)
						values.put(dbField.fieldName, (Long) dbField.toSqlVal());
					else if(dbField.toSqlVal().getClass()==Integer.class)
						values.put(dbField.fieldName, (Integer) dbField.toSqlVal());
				}
				else if(type==MBDatabaseFieldType.TEXT)
				{
					values.put(dbField.fieldName, (String) dbField.toSqlVal());
				}
			}
		}
		long newId = parent.getDb().insert(tableName(), null, values);
		parent.closeDb();
		this.id.val(newId);
		return newId;
	}
	
	public boolean update()
	{
		ContentValues values = new ContentValues();
		fields = getMBFields();
		for (Field field : fields) {
			MBDatabaseField<?> dbField = null;
			try {
				dbField = ((MBDatabaseField<?>) field.get(this));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			if(dbField.val() == null)
			{
				values.putNull(dbField.fieldName);
			}
			else
			{
				String type = dbField.type;
				if(type==MBDatabaseFieldType.INTEGER)
				{
					if(dbField.toSqlVal().getClass()==Long.class)
						values.put(dbField.fieldName, (Long) dbField.toSqlVal());
					else if(dbField.toSqlVal().getClass()==Integer.class)
						values.put(dbField.fieldName, (Integer) dbField.toSqlVal());
				}
				else if(type==MBDatabaseFieldType.TEXT)
				{
					values.put(dbField.fieldName, (String) dbField.toSqlVal());
				}
			}
		}
		int numRows = parent.getDb().update(tableName(), values, "id = "+this.id.val(), null);
		parent.closeDb();
		return (numRows==1);
	}
	
	public Long save()
	{
		boolean updateReturn = false;
		if(this.id.val()==MBDatabase.ID_UNSET)
		{
			this.id.val(null);
			return insert();
		}
		else
			updateReturn = update();
		if(updateReturn)
			return this.id.val();
		else
			return null;
	}
	
	public boolean delete()
	{
		int num = parent.getDb().delete(tableName(), "id = "+this.id.val(), null);
		if(num == 0)
		{
			return false;
		}
		else
		{
			emptyModel();
			return true;
		}
	}
	
	public void emptyModel()
	{
		fields = getMBFields();
		for (Field field : fields) {
			try {
				((MBDatabaseField<?>)field.get(this)).val(null);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		
	}
}
