package com.mainboardlib.orm;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper extends SQLiteOpenHelper {

	private MBDatabase parent;
	
	public SQLHelper(MBDatabase p) {
		super(MBDatabase.context, p.dbName(), null, p.dbVersion());
		parent = p;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		parent.onCreateDatabase(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		parent.onUpdateDatabase(db);
	}

}
