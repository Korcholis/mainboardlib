package com.mainboardlib.orm;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import com.mainboardlib.exceptions.ModelNotSetException;
import com.mainboardlib.exceptions.NotAModelException;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

/**
 * @author kor 
 * 
 * Class representation for a SQLite Database. It's able to hold
 * MBModel elements inside as tables.
 * This is an abstract class, and always needs to be overriden.
 * @see MBModel
 */
public abstract class MBDatabase extends ContentProvider {

	public static final Long ID_UNSET = -1l;
	protected HashMap<String, MBModel> models = new HashMap<String, MBModel>();
	protected SQLiteDatabase db;
	protected SQLHelper sqlHelper;
	public static Context context;

	/**
	 * Gets the authority URI path to be used by MBDatabase
	 * @return String The authority URI path
	 */
	public abstract String authority();

	/**
	 * Gets the Database filename. It should always end with .db
	 * @return String The Database filename
	 */
	public abstract String dbName();

	/**
	 * Gets the numeric version for this database.
	 * @return int The numeric Database version
	 */
	public abstract int dbVersion();

	/**
	 * Constructor for MBDatabase
	 */
	public MBDatabase() {
		super();
		MBModel.setParent(this);
	}

	/**
	 * Gets a MBModel from the Database. If it doesn't exist, the system throws an exception
	 * @param modelName The chosen MBModel name
	 * @return MBModel the model you selected.
	 * @throws ModelNotSetException when the model you chose is not in MBDatabase.
	 * @see MBModel
	 * @see ModelNotSetException
	 */
	public MBModel model(String modelName) throws ModelNotSetException {
		MBModel model = models.get(modelName);
		if (model == null)
			throw new ModelNotSetException();
		else
			return model;
	}

	/**
	 * Adds a MBModel to the Database. When the object is not an instance of MBModel, it throws a NotAModelException
	 * @param t The model you want to add to MBDatabase
	 * @throws NotAModelException when the passed argument is not a MBModel
	 * @see MBModel
	 * @see NotAModelException
	 */
	public void addModel(MBModel t) throws NotAModelException {
		if (!isModel(t))
			throw new NotAModelException();
		models.put(t.tableName(), t);
	}

	/**
	 * Tests if the selected object is a MBModel instance or not.
	 * @param t The MBModel you want to test
	 * @return boolean true if t is a MBModel, false if it isn't.
	 */
	private boolean isModel(MBModel t) {
		return (t.getClass().getSuperclass().getName().equals(MBModel.class
				.getName()));
	}

	/**
	 * Method called when the Database is created for the first time.
	 * @param db The SQLiteDatabase object used to make the SQLite calls
	 * @see SQLiteDatabase
	 */
	public void onCreateDatabase(SQLiteDatabase db) {
		for (Iterator<Entry<String, MBModel>> iterator = models.entrySet()
				.iterator(); iterator.hasNext();) {
			Entry<String, MBModel> t = (Entry<String, MBModel>) iterator.next();
			t.getValue().onCreateTable(db);
		}
	}

	/**
	 * Method called when the Database is updated by the system.
	 * @param db The SQLiteDatabase object used to make the SQLite calls
	 * @see SQLiteDatabase
	 */
	public void onUpdateDatabase(SQLiteDatabase db) {

	}

	/* (non-Javadoc)
	 * @see android.content.ContentProvider#delete(android.net.Uri, java.lang.String, java.lang.String[])
	 */
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}

	/* (non-Javadoc)
	 * @see android.content.ContentProvider#getType(android.net.Uri)
	 */
	@Override
	public String getType(Uri uri) {
		return null;
	}

	/* (non-Javadoc)
	 * @see android.content.ContentProvider#insert(android.net.Uri, android.content.ContentValues)
	 */
	@Override
	public Uri insert(Uri uri, ContentValues values) {
		return null;
	}

	/* (non-Javadoc)
	 * @see android.content.ContentProvider#onCreate()
	 */
	@Override
	public boolean onCreate() {
		setDb();
		return true;
	}

	/**
	 * Method used to set the Database with the current configuration.
	 */
	public void setDb() {
		sqlHelper = new SQLHelper(this);
		MBModel.setParent(this);
	}

	/**
	 * Gets the {@link SQLiteDatabase} object to make SQLite calls.
	 * @return a SQLiteDatabase object
	 * @see SQLiteDatabase
	 */
	public SQLiteDatabase getDb() {
		if (db == null) {
			if (sqlHelper == null) {
				setDb();
			}
			db = sqlHelper.getWritableDatabase();
		}
		return db;
	}

	/**
	 * Manually closes the {@link SQLiteDatabase} object instance and sets it to null.
	 */
	public void closeDb() {
		db.close();
		db = null;
	}

	/* (non-Javadoc)
	 * @see android.content.ContentProvider#query(android.net.Uri, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String)
	 */
	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		return null;
	}

	/* (non-Javadoc)
	 * @see android.content.ContentProvider#update(android.net.Uri, android.content.ContentValues, java.lang.String, java.lang.String[])
	 */
	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		return 0;
	}
}
