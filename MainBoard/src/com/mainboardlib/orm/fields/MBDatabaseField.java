package com.mainboardlib.orm.fields;

import java.util.ArrayList;

import com.mainboardlib.exceptions.MethodNotUsableException;

public abstract class MBDatabaseField<T> {

	protected T value;
	public String type;
	public T defaultValue;
	public boolean isForeignKey;
	public boolean isPrimaryKey;
	public String fieldName;
	public Class<?> foreignClass;
	public boolean isUnique;
	public boolean isReference = false;
	public ArrayList<String> validators;
	
	public void val(T val)
	{
		value = val;
	}
	
	public T val()
	{
		return value;
	}
	
	public String toCreateString()
	{
		String query = fieldName + " " + type;
		if(isPrimaryKey)
			query += " PRIMARY KEY AUTOINCREMENT";
		else if(isUnique)
			query += " UNIQUE";
		return query;
	}
	
	public abstract void fromViewString(String val) throws MethodNotUsableException;
	
	public abstract String toViewString() throws MethodNotUsableException;
	
	public abstract Object toSqlVal();
	public abstract void fromSqlVal(Object val);
	
	
}
