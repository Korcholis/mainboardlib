package com.mainboardlib.orm.fields;

import com.mainboardlib.exceptions.MethodNotUsableException;
import com.mainboardlib.orm.MBModel;

public class MBForeignField<E> extends MBDatabaseField<E> {
	
	public MBForeignField(String fieldName, Class<?> foreignClass)
	{
		type = MBDatabaseFieldType.INTEGER;
		this.fieldName = fieldName;
		isForeignKey = true;
		this.foreignClass = foreignClass;
		isPrimaryKey = false;
		isUnique = false;
	}
	
	public String toSqlFkString()
	{
		try {
			return "FOREIGN KEY ("+fieldName+") REFERENCES "+((MBModel)foreignClass.newInstance()).tableName()+"(id)";
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public void fromViewString(String val) throws MethodNotUsableException {
		throw new MethodNotUsableException();
	}

	@Override
	public String toViewString() throws MethodNotUsableException {
		throw new MethodNotUsableException();
	}

	@Override
	public Object toSqlVal() {
		return ((MBModel)this.value).id.toSqlVal();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void fromSqlVal(Object val) {
		try {
			this.value = (E) ((MBModel) this.foreignClass.newInstance()).selectById((Long) val);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}
	}
}
