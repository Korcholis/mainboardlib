package com.mainboardlib.orm.fields;

import com.mainboardlib.exceptions.MethodNotUsableException;

public class MBBoolean extends MBDatabaseField<Boolean> {

	public MBBoolean(String fieldName, Boolean defaultValue, boolean isPk, boolean isUnique)
	{
		type = MBDatabaseFieldType.INTEGER;
		this.fieldName = fieldName;
		this.defaultValue = defaultValue;
		isForeignKey = false;
		isPrimaryKey = isPk;
		this.isUnique = isUnique|isPk;
	}
	
	@Override
	public void fromViewString(String val) throws MethodNotUsableException {
		this.value = Boolean.parseBoolean(val);
	}

	@Override
	public String toViewString() throws MethodNotUsableException {
		return this.value.toString();
	}

	@Override
	public Object toSqlVal() {
		return (this.value==true)? 1:0;
	}

	@Override
	public void fromSqlVal(Object val) {
		this.value = ((Integer) val == 1)? true : false;
	}
}
