package com.mainboardlib.orm.fields;

public class MBInt extends MBDatabaseField<Integer> {
	
	public MBInt(String fieldName, Integer defaultValue, boolean isPk, boolean isUnique)
	{
		type = MBDatabaseFieldType.INTEGER;
		this.fieldName = fieldName;
		this.defaultValue = defaultValue;
		isForeignKey = false;
		isPrimaryKey = isPk;
		this.isUnique = isUnique|isPk;
	}

	@Override
	public void fromViewString(String val) {
		this.value = Integer.parseInt(val);
	}

	@Override
	public String toViewString() {
		return this.value+"";
	}

	@Override
	public Object toSqlVal() {
		return this.value;
	}

	@Override
	public void fromSqlVal(Object val) {
		this.value = (Integer) val;
	}
}
