package com.mainboardlib.orm.fields;

import com.mainboardlib.exceptions.MethodNotUsableException;

public class MBString extends MBDatabaseField<String> {
	
	public MBString(String fieldName, String defaultValue, boolean isPk, boolean isUnique)
	{
		type = MBDatabaseFieldType.TEXT;
		this.fieldName = fieldName;
		this.defaultValue = defaultValue;
		isForeignKey = false;
		isPrimaryKey = isPk;
		this.isUnique = isUnique|isPk;
	}

	@Override
	public void fromViewString(String val) throws MethodNotUsableException {
		this.value = val; 
	}

	@Override
	public String toViewString() throws MethodNotUsableException {
		return this.value;
	}

	@Override
	public Object toSqlVal() {
		return this.value;
	}

	@Override
	public void fromSqlVal(Object val) {
		this.value = (String) val;
	}
}
