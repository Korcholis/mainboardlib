package com.mainboardlib.orm.fields;

import com.mainboardlib.exceptions.MethodNotUsableException;

public class MBLong extends MBDatabaseField<Long> {
	
	public MBLong(String fieldName, Long defaultValue, boolean isPk, boolean isUnique)
	{
		type = MBDatabaseFieldType.INTEGER;
		this.fieldName = fieldName;
		this.defaultValue = defaultValue;
		isForeignKey = false;
		isPrimaryKey = isPk;
		this.isUnique = isUnique|isPk;
	}

	@Override
	public void fromViewString(String val) throws MethodNotUsableException {
		this.value = Long.parseLong(val);
	}

	@Override
	public String toViewString() throws MethodNotUsableException {
		return this.value+"";
	}

	@Override
	public Object toSqlVal() {
		return this.value;
	}

	@Override
	public void fromSqlVal(Object val) {
		this.value = (Long) val;
	}
}
