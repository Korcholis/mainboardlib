package com.mainboardlib.orm.fields;

import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.mainboardlib.exceptions.MethodNotUsableException;

public class MBDate extends MBDatabaseField<Calendar> {

	public MBDate(String fieldName, Calendar defaultValue, boolean isPk, boolean isUnique)
	{
		type = MBDatabaseFieldType.INTEGER;
		this.fieldName = fieldName;
		this.defaultValue = defaultValue;
		isForeignKey = false;
		isPrimaryKey = isPk;
		this.isUnique = isUnique|isPk;
	}
	
	@Override
	public void fromViewString(String val) throws MethodNotUsableException {
		this.value = GregorianCalendar.getInstance();
		this.value.setTime(Date.valueOf(val));
	}

	@Override
	public String toViewString() throws MethodNotUsableException {
		return this.val().toString();
	}

	@Override
	public Object toSqlVal() {
		return this.value.getTimeInMillis();
	}

	@Override
	public void fromSqlVal(Object val) {
		this.value = GregorianCalendar.getInstance();
		this.value.setTimeInMillis((Long) val);
	}

}
