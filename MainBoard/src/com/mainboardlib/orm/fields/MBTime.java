package com.mainboardlib.orm.fields;


import com.mainboardlib.exceptions.MethodNotUsableException;

public class MBTime extends MBDatabaseField<Long> {

	public MBTime(String fieldName, Long defaultValue, boolean isUnique)
	{
		type = MBDatabaseFieldType.INTEGER;
		this.fieldName = fieldName;
		this.defaultValue = defaultValue;
		isForeignKey = false;
		isPrimaryKey = false;
		this.isUnique = isUnique;
	}
	
	@Override
	public void fromViewString(String val) throws MethodNotUsableException {
		this.value = Long.parseLong(val);
	}

	@Override
	public String toViewString() throws MethodNotUsableException {
		int tempTime = (int) (this.value/1000);
		int seconds = tempTime%60;
		tempTime/=60;
		int minutes = tempTime%60;
		int hours = tempTime/60;
		String sMins = String.valueOf(minutes);
		String sSecs = String.valueOf(seconds);
		if(sMins.length()<2)
			sMins = "0"+sMins;
		if(sSecs.length()<2)
			sSecs = "0"+sSecs;
		String tempString = hours+":"+sMins+":"+sSecs;
		return tempString;
	}

	@Override
	public Object toSqlVal() {
		return this.value;
	}

	@Override
	public void fromSqlVal(Object val) {
		this.value = (Long) val;
	}

}
