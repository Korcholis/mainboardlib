package com.mainboardlib.orm.fields;

import java.util.Vector;

import com.mainboardlib.exceptions.MethodNotUsableException;
import com.mainboardlib.orm.MBModel;

public class MBHasSome<E> extends MBDatabaseField<Vector<E>> {
	
	protected MBModel parent;
	
	public MBHasSome(String fieldName, Class<?> className, MBModel parent)
	{
		this.parent = parent;
		this.foreignClass = className;
		this.fieldName = fieldName;
		this.isReference = true;
		this.value = new Vector<E>();
		this.type = "REFERENCE"; //Should not be tracked in insert steps
	}
	
	@Override
	public void fromViewString(String val) throws MethodNotUsableException {
		throw new MethodNotUsableException();
	}

	@Override
	public String toViewString() throws MethodNotUsableException {
		throw new MethodNotUsableException();
	}

	@Override
	public Object toSqlVal() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void fromSqlVal(Object val) {
		try {
			this.value = (Vector<E>) ((MBModel) this.foreignClass.newInstance()).select(this.fieldName+" = "+this.parent.id.val());
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}
	}
}
