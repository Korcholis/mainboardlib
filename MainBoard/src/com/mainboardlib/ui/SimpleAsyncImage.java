package com.mainboardlib.ui;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

/**
 * @author Sergi Juanola
 **/

public class SimpleAsyncImage extends AsyncTask<String, Void, Bitmap> {

	private String url;
	private final WeakReference<?> imageViewReference;
	private int progressIcon = android.R.drawable.progress_indeterminate_horizontal;

	public SimpleAsyncImage(ImageView imageView) {
		imageViewReference = new WeakReference<ImageView>(imageView);
	}
	
	public SimpleAsyncImage(ImageView imageView, int progressResource) {
		imageViewReference = new WeakReference<ImageView>(imageView);
		progressIcon = progressResource;
	}

	@Override
	protected Bitmap doInBackground(String... params) {
		url = params[0];
		try {
			return BitmapFactory.decodeStream(new URL(url).openConnection()
					.getInputStream());
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		if (isCancelled()) {
			result = null;
		}
		if (imageViewReference != null) {
			ImageView imageView = (ImageView) imageViewReference.get();
			if (imageView != null) {
				imageView.setImageBitmap(result);
			}
		}
	}

	@Override
	protected void onPreExecute() {
		if (imageViewReference != null) {
			ImageView imageView = (ImageView) imageViewReference.get();
			if (imageView != null) {
				imageView.setImageResource(progressIcon);
			}
		}
	}
}