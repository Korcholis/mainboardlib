package com.mainboardlib.ui;

import java.util.ArrayList;

import android.content.Intent;
import android.speech.RecognizerIntent;

public class CommandManager {
	private ArrayList<Command> commands;
	
	public CommandManager()
	{
		commands = new ArrayList<Command>();
	}
	
	public void addCommand(String pattern, int code)
	{
		commands.add(new Command(code, pattern));
	}
	
	public Intent generateIntent(String prompt, int results)
	{
		Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, prompt);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
        return intent;
	}
	
	public Command getCommand(Intent data)
	{
		ArrayList<String> matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
		for (String match : matches) {
			for (Command possibleCommand : commands) {
				if(possibleCommand.matches(match))
				{
					return possibleCommand;
				}
			}
		}
		return null;
	}
}
