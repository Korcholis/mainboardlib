package com.mainboardlib.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Command {
	private int mCode;
	private String mOriginalPattern;
	private Pattern mDigestedPattern;
	private ArrayList<String> mLabels;
	private Map<String, String> mVariables;
	private Pattern wildcard = Pattern.compile("@([a-zA-Z]+[a-zA-Z0-9]*)");
	
	public Command(int code, String pattern)
	{
		mLabels = new ArrayList<String>();
		mLabels.add("wholeMatch");
		processPattern(pattern);
		mCode = code;
	}
	
	public String originalPattern()
	{
		return mOriginalPattern;
	}
	
	private void processPattern(String pattern) {
		mOriginalPattern = pattern;
		Matcher match = wildcard.matcher(pattern);
		while(match.find())
		{
			mLabels.add(match.group(1));
		}
		mDigestedPattern = Pattern.compile(match.replaceAll("(.*)"));
	}

	public int code() {
		return mCode;
	}
	
	public void code(int code) {
		this.mCode = code;
	}
	
	public void addVariable(String label, String variable)
	{
		mVariables.put(label, variable);
	}
	public void addVariable(int position, String variable)
	{
		mVariables.put(mLabels.get(position), variable);
	}
	
	public Map<String, String> variables()
	{
		return mVariables;
	}
	
	public String getVariable(String key)
	{
		if(mVariables.containsKey(key))
		{
			return mVariables.get(key);
		}
		return null;
	}
	
	public boolean matches(String command)
	{
		Matcher matcher = mDigestedPattern.matcher(command);
		if(matcher.matches())
		{
			mVariables = new HashMap<String, String>();
			while(matcher.find())
			{
				for (int i = 0; i <= matcher.groupCount(); i++) {
					addVariable(i, matcher.group(i));
				}
			}
			return true;
		}
		return false;
	}
}
