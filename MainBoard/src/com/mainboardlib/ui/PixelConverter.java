package com.mainboardlib.ui;

import android.content.Context;

public class PixelConverter {

	private final float scale;
	private final float add = 0.5f;
	
	public PixelConverter(Context context)
	{
		scale = (float)context.getResources().getDisplayMetrics().density;
	}
	
	public int toDip(float pixels)
	{
		return (int)(pixels * scale + add);
	}
}
