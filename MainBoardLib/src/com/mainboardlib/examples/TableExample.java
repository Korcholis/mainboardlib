package com.mainboardlib.examples;

import com.mainboardlib.orm.MBDatabaseTable;
import com.mainboardlib.orm.fields.MBForeignField;
import com.mainboardlib.orm.fields.MBString;

public class TableExample extends MBDatabaseTable {
	public MBString name = new MBString("name", null, false, false);
	public MBString email = new MBString("email", null, false, false);
	public MBForeignField<ForeignTable> foreignTable = new MBForeignField<ForeignTable>("foreign_id", ForeignTable.class);

	@Override
	public String tableName()
	{
		return "tableexample";
	}
}
