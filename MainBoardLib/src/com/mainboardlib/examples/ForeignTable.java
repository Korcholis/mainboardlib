package com.mainboardlib.examples;

import com.mainboardlib.orm.MBDatabaseTable;

public class ForeignTable extends MBDatabaseTable {

	@Override
	public String tableName()
	{
		return "foreigntable";
	}
}
