package com.mainboardlib.examples;

import com.mainboardlib.orm.MBDatabase;
import com.mainboardlib.test.ITimeTesterCode;
import com.mainboardlib.test.TimeTester;


public class Program {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		TimeTester tester = new TimeTester("Create DB", new ITimeTesterCode() {
			@Override
			public void codeGoesHere() {
				MBDatabase db = new MBDatabase();
				db.addTable(new ForeignTable());
				db.addTable(new TableExample());
				db.onCreateDatabase();
			}
		}, TimeTester.SLOWEST_MOST_ACCURATE);
		tester.test();
		
		
	}

}
