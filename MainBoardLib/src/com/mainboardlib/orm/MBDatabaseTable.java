package com.mainboardlib.orm;

import java.lang.reflect.Field;
import java.util.ArrayList;

import com.mainboardlib.orm.fields.MBDatabaseField;
import com.mainboardlib.orm.fields.MBForeignField;
import com.mainboardlib.orm.fields.MBInt;

public abstract class MBDatabaseTable {
	
	public MBInt id = new MBInt("id", null, true, true);
	private static final Class<?> __classHolder = new MBDatabaseField<String>().getClass();
	private static MBDatabase db = null;
	
	public static MBDatabase getDatabase() {
		return db;
	}

	public static void setDatabase(MBDatabase database)
	{
		db = database;
	}
	
	public abstract String tableName();
	
	@SuppressWarnings("unchecked")
	public String onCreateTable()
	{
		ArrayList<Field> mbFields = getMBFields();
		StringBuilder query = new StringBuilder();
		StringBuilder fkQuery = new StringBuilder();
		query.append("CREATE TABLE "+tableName()+"(\n");
		boolean notFirst = false;
		for (Field field : mbFields) {
			try {
				if(notFirst)
			        query.append(",\n");
				else
					notFirst = true;
				query.append("\t"+((MBDatabaseField<Integer>)field.get(this)).toSqlString());
				if(field.getType() == MBForeignField.class)
				{
					if(notFirst)
				        fkQuery.append(",\n");
					else
						notFirst = true;
					fkQuery.append("\t"+((MBForeignField<Integer>)field.get(this)).toSqlFkString());
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		query.append(fkQuery).append("\n);");
		return query.toString();
	}
	
	private ArrayList<Field> getMBFields()
	{
		Field[] fields = this.getClass().getFields();
		ArrayList<Field> mbFields = new ArrayList<Field>();
		for (int i = 0; i < fields.length; i++) {
			if(fields[i].getType().getSuperclass() == __classHolder)
			{
				mbFields.add(fields[i]);
			}
		}
		return mbFields;
	}
}
