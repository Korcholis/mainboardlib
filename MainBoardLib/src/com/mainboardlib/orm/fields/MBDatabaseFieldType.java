package com.mainboardlib.orm.fields;

public class MBDatabaseFieldType {
	public static final String NULL = "NULL";
	public static final String INTEGER = "INTEGER";
	public static final String REAL = "REAL";
	public static final String TEXT = "TEXT";
	public static final String BLOB = "BLOB";
}
