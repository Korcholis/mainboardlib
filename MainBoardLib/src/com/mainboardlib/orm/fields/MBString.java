package com.mainboardlib.orm.fields;

public class MBString extends MBDatabaseField<String> {
	
	public MBString(String fieldName, String defaultValue, boolean isPk, boolean isUnique)
	{
		type = MBDatabaseFieldType.TEXT;
		this.fieldName = fieldName;
		this.defaultValue = defaultValue;
		isForeignKey = false;
		isPrimaryKey = isPk;
		this.isUnique = isUnique|isPk;
	}
}
