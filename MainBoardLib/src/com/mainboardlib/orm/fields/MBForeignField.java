package com.mainboardlib.orm.fields;

import java.util.ArrayList;

import com.mainboardlib.orm.MBDatabaseTable;

public class MBForeignField<E> extends MBDatabaseField<ArrayList<E>> {
	
	public MBForeignField(String fieldName, Class<?> foreignClass)
	{
		type = MBDatabaseFieldType.INTEGER;
		this.fieldName = fieldName;
		isForeignKey = true;
		this.foreignClass = foreignClass;
		isPrimaryKey = false;
		isUnique = false;
	}
	
	public String toSqlFkString()
	{
		try {
			return "FOREIGN KEY ("+fieldName+") REFERENCES "+((MBDatabaseTable)foreignClass.newInstance()).tableName()+"(id)";
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return "";
	}
}
