package com.mainboardlib.orm.fields;

import java.util.ArrayList;

public class MBDatabaseField<T> {

	protected T value;
	protected String type;
	public T defaultValue;
	public boolean isForeignKey;
	public boolean isPrimaryKey;
	public String fieldName;
	public Class<?> foreignClass;
	public boolean isUnique;
	public ArrayList<String> validators;
	
	public void val(T val)
	{
		value = val;
	}
	
	public T val()
	{
		return value;
	}
	
	public String toSqlString()
	{
		String query = fieldName + " " + type;
		if(isPrimaryKey)
			query += " PRIMARY KEY";
		else if(isUnique)
			query += " UNIQUE";
		return query;
	}
}
