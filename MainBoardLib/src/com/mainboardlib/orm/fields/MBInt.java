package com.mainboardlib.orm.fields;

public class MBInt extends MBDatabaseField<Integer> {
	
	public MBInt(String fieldName, Integer defaultValue, boolean isPk, boolean isUnique)
	{
		type = MBDatabaseFieldType.INTEGER;
		this.fieldName = fieldName;
		this.defaultValue = defaultValue;
		isForeignKey = false;
		isPrimaryKey = isPk;
		this.isUnique = isUnique|isPk;
	}
}
