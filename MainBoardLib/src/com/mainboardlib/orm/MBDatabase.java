package com.mainboardlib.orm;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class MBDatabase {
	private HashMap<String, MBDatabaseTable> tables = new HashMap<String, MBDatabaseTable>();
	
	@SuppressWarnings("static-access")
	public boolean addTable(MBDatabaseTable t)
	{
		if(!isTable(t))
			return false;
		t.setDatabase(this);
		tables.put(t.tableName(), t);
		return true;
	}
	
	private boolean isTable(MBDatabaseTable t)
	{
		return (t.getClass().getSuperclass().getName() == MBDatabaseTable.class.getName());
	}
	
	public void onCreateDatabase()
	{
		for (Iterator<Entry<String, MBDatabaseTable>> iterator = tables.entrySet().iterator(); iterator.hasNext();) {
			Entry<String, MBDatabaseTable> t = (Entry<String, MBDatabaseTable>) iterator.next();
			t.getValue().onCreateTable();
		}
	}
}
