package com.mainboardlib.test;

public class TimeTester {
	public static final int JUST_ONCE = 1;
	public static final int FAST_INACCURATE = 10;
	public static final int SLOW_ACCURATE = 1000;
	public static final int VERY_SLOW_VERY_ACCURATE = 100000;
	public static final int SLOWEST_MOST_ACCURATE = 10000000;
	
	private Runnable task;
	private int iterations = JUST_ONCE;
	private String taskName = "";
	private ITimeTesterCode internalCode;
	
	public TimeTester(String taskName, ITimeTesterCode code)
	{
		this.taskName = taskName;
		this.internalCode = code;
	}
	
	public TimeTester(String taskName, ITimeTesterCode code, int iterations)
	{
		this.taskName = taskName;
		this.internalCode = code;
		this.iterations = iterations;
	}
	
	public void test()
	{
		task = new Runnable() {
			
			@Override
			public void run() {
				long t1 = System.currentTimeMillis();
				for (int i = 0; i < iterations; i++) {
					internalCode.codeGoesHere();
				}
				long t2 = System.currentTimeMillis();
				System.out.println("[TimeTester] "+taskName+": it took " + (float)(t2 - t1)/iterations + " milliseconds to execute (on average).");
			}
		};
		task.run();
	}
}
